
public class Main {

    public static void main(String[] args) {

        // Del A

        // Skapa ett objekt av klassen mha standardkonstruktorn (implicit angiven)
        DelA delA = new DelA();

        // Anropa utskriftsfunktionen (stigande talserie)
        delA.printRange(36, 42);

        // Anropa utskriftsfunktionen (fallande talserie)
        delA.printRange(42, 35);

        // Anropa utskriftsfunktionen (talserie med endast ett tal)
        delA.printRange(21, 21);


        // Del B

        // Skapa ett objekt av klassen mha standardkonstruktorn (explicit angiven)
        DelB delB_1 = new DelB();

        // Ange steglängd via setter
        delB_1.setStep(3);

        // Anropa utskriftsfunktionen (stigande talserie)
        delB_1.printRange(4, 18);

        // Ange multipel för infälld text (ovan angiven steglängd kvarstår)
        delB_1.setMultipel(5);

        // Anropa utskriftsfunktionen (fallande talserie)
        delB_1.printRange(18, 4);


        // Skapa ett objekt av klassen mha konstruktor med parametrar
        DelB delB_2 = new DelB(3, 4);

        // Anropa utskriftsfunktionen
        delB_2.printRange(2, 15);


        // Del C

        // Skapa ett objekt av klassen mha standardkonstruktorn
        DelC delC = new DelC(2, 0);

        // Anropa den utökade utskriftsfunktionen
        delC.printRange(11, 17, 3);

        // Ange multipel för infälld text (steglängd angiven i konstruktorn kvarstår)
        delC.setMultipel(3);

        // Skriv ut en talserie (med infälld text)
        delC.printRange(3, 15);

        // Hur många gånger skrevs ersättningsstängen ut?
        delC.countString();
    }
}
