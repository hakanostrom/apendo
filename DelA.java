
/**
 * Apendo programmeringstest Del A
 * Håkan Öström
 * hakan.ostrom@gmail.com
 */

public class DelA {

    // Defaultkonstruktorn finns implicit eftersom inga andra konstruktorer angetts

    /**
     * Funktion för att skriva ut talserie
     *
     * @param start Startvärdet för talserien
     * @param stop  Slutvärdet för talserien
     */
    public void printRange(int start, int stop) {

        // Avgör först vilket av start eller stop som är störst (stigande eller fallande talserie)
        if (start < stop) {
            // Använd en for-loop för att stega från start till stop, skriv ut varje steg
            for (int i = start; i <= stop; i++) {
                System.out.print(i + " ");
            }
        } else {
            // Använd en for-loop för att stega från start till stop, skriv ut varje steg
            for (int i = start; i >= stop; i--) {
                System.out.print(i + " ");
            }
        }

        // Skriv ut nyrad för formateringens skull
        System.out.println("");
    }
}
