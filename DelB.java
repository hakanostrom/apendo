
/**
 * Apendo programmeringstest Del B
 * Håkan Öström
 * hakan.ostrom@gmail.com
 */

public class DelB {

    // Steglängden i talserien (heltal)
    private int step;

    // Ersätt utskrift av tal i talserien om talet är en jämn multipel av denna variabel
    private int multipel;


    /**
     * Standardkonstruktor utan argument
     * Sätter steglängd och multipel till standardvärden.
     */
    public DelB() {
        step = 1;
        multipel = 0;
    }

    /**
     * Konstruktor där båda medlemsvariablerna sätts
     *
     * @param step     Steglängd
     * @param multipel Multipel för textersättning av steg
     */
    public DelB(int step, int multipel) {
        this.step = step;
        this.multipel = multipel;
    }

    // Getter för step
    public int getStep() {
        return step;
    }

    // Setter för step
    public void setStep(int step) {
        this.step = step;
    }

    // Getter för multipel
    public int getMultipel() {
        return multipel;
    }

    // Setter för multipel
    public void setMultipel(int multipel) {
        this.multipel = multipel;
    }

    /**
     * Funktion för att skriva ut talserie
     *
     * @param start Startvärde för talserien
     * @param stop  Slutvärde för talserien
     */
    public void printRange(int start, int stop) {

        // Avgör först vilket av start eller stop som är störst (stigande eller fallande talserie)
        if (start < stop) {

            // Använd en for-loop för att stega från start till stop, skriv ut varje steg. Stegandet sker stigande.
            for (int i = start; i <= stop; i = i + step) {

                // Skriv ut enskilda steg
                // Om multipeln av steget är satt till något annat än noll (defaultvärdet) samt är lika med steget (dvs resten vid heltalsdivision, modulus, är noll)
                // så skriv istället ut en text
                if (multipel != 0 && i % multipel == 0)
                    System.out.print("Apendo ");
                else
                    System.out.print(i + " ");
            }
        } else {

            // Använd en for-loop för att stega från start till stop, skriv ut varje steg. Stegandet sker fallande.
            for (int i = start; i >= stop; i = i - step) {

                // Skriv ut enskilda steg
                // Om multipeln av steget är satt till något annat än noll (defaultvärdet) samt är lika med steget (dvs resten vid heltalsdivision, modulus, är noll)
                // så skriv istället ut en text
                if (multipel != 0 && i % multipel == 0)
                    System.out.print("Apendo ");
                else
                    System.out.print(i + " ");

            }
        }

        // Skriv ut nyrad för formateringens skull
        System.out.println("");

    }

}
