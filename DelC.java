
/**
 * Apendo programmeringstest Del C
 * Håkan Öström
 * hakan.ostrom@gmail.com
 */

public class DelC {

    // Steglängden i talserien (heltal)
    private int step;

    // Ersätt utskrift av tal i talserien om talet är en jämn multipel av denna variabel
    private int multipel;

    // Håll reda på hur många gånger ersätnningssträngen skrivits ut
    private int stringCount;

    /**
     * Standardkonstruktor utan argument
     * Sätter steglängd och multipel till standardvärden.
     */
    public DelC() {

        step = 1;
        multipel = 0;

    }

    /**
     * Konstruktor där båda medlemsvariablerna step och multipel sätts
     *
     * @param step     Steglängd
     * @param multipel Multipel för textersättning av steg
     */
    public DelC(int step, int multipel) {
        this.step = step;
        this.multipel = multipel;
    }

    // Getter för step
    public int getStep() {
        return step;
    }

    // Setter för step
    public void setStep(int step) {
        this.step = step;
    }

    // Getter för multipel
    public int getMultipel() {
        return multipel;
    }

    // Setter för multipel
    public void setMultipel(int multipel) {
        this.multipel = multipel;
    }

    /**
     * Funktion för att skriva ut talserie
     *
     * @param start Startvärde för talserien
     * @param stop  Slutvärde för talserien
     */
    public void printRange(int start, int stop) {

        // Avgör först vilket av start eller stop som är störst  (stigande eller fallande talserie)
        if (start < stop) {

            // Använd en for-loop för att stega från start till stop, skriv ut varje steg. Stegandet sker stigande.
            for (int i = start; i <= stop; i = i + step) {

                // Skriv ut enskilda steg
                // Om multipeln av steget är satt till något annat än noll (defaultvärdet) samt är lika med steget (dvs resten vid heltalsdivision, modulus, är noll)
                // så skriv istället ut en text
                if (multipel != 0 && i % multipel == 0) {
                    System.out.print("Apendo ");
                    // Öka räknaren ett steg
                    stringCount++;
                } else
                    System.out.print(i + " ");
            }
        } else {

            // Använd en for-loop för att stega från start till stop, skriv ut varje steg. Stegandet sker fallande.
            for (int i = start; i >= stop; i = i - step) {

                // Skriv ut enskilda steg
                // Om multipeln av steget är satt till något annat än noll (defaultvärdet) samt är lika med steget (dvs resten vid heltalsdivision, modulus, är noll)
                // så skriv istället ut en text
                if (multipel != 0 && i % multipel == 0) {
                    System.out.print("Apendo ");
                    // Öka räknaren ett steg
                    stringCount++;
                } else
                    System.out.print(i + " ");

            }
        }

        // Skriv ut nyrad för formaterings skull
        System.out.println("");

    }

    /**
     * Överlagra printRange-funktionen med en variant som tar tre argument
     *
     * @param start  Startvärde för talserien
     * @param stop   Slutvärde för talserien
     * @param jumpTo Ordningsnummret för den plats i talserien som skall skrivas ut.
     */
    public void printRange(int start, int stop, int jumpTo) {

        // Räknandet börjar på noll
        stringCount = 0;

        // Beräkningen nedan kräver att variabeln är ett mindre än vad som anges "i talspråk"
        // Exempelvis om första talet skall skrivas ut så skall noll steg framåt göras
        jumpTo = jumpTo - 1;

        // Avgör först vilket av start eller stop som är störst (stigande eller fallande talserie)
        if (start < stop) {

            // Använd en for-loop för att stega från start till stop, skriv ut varje steg. Stegandet sker stigande.
            for (int i = start; i <= stop; i = i + step) {

                // Skriv en endast ut om loopen kommit till den plats som angivits av single
                if (i == start + jumpTo * step) {
                    // Skriv ut enskilda steg
                    // Om multipeln av steget är satt till något annat än noll (defaultvärdet) samt är lika med steget (dvs resten vid heltalsdivision, modulus, är noll)
                    // så skriv istället ut en text
                    if (multipel != 0 && i % multipel == 0) {
                        System.out.print("Apendo ");
                        // Öka räknaren ett steg
                        stringCount++;
                    } else
                        System.out.print(i + " ");
                }


            }
        } else {

            // Använd en for-loop för att stega från start till stop, skriv ut varje steg. Stegandet sker fallande.
            for (int i = start; i >= stop; i = i - step) {

                // Skriv en endast ut om loopen kommit till den plats som angivits av single
                if (i == start + jumpTo * step) {
                    // Skriv ut enskilda steg
                    // Om multipeln av steget är satt till något annat än noll (defaultvärdet) samt är lika med steget (dvs resten vid heltalsdivision, modulus, är noll)
                    // så skriv istället ut en text
                    if (multipel != 0 && i % multipel == 0) {
                        System.out.print("Apendo ");
                        // Öka räknaren ett steg
                        stringCount++;
                    } else
                        System.out.print(i + " ");
                }

            }
        }

        // Skriv ut nyrad för formateringens skull
        System.out.println("");

    }

    /**
     * Funktion som skriver ut antal gånger som ett tal i talserien bytts ut mot en sträng
     */
    public void countString() {
        System.out.println("Apendo skrevs ut " + stringCount + " gånger i föregående talserie.");
    }

}
